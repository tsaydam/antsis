/*
 * bLed.h
 *
 *  Created on: 18 A�u 2015
 *      Author: Talha SAYDAM
 */

#ifndef BLED_H_
#define BLED_H_

#include <ti/drivers/PIN.h>

extern PIN_State pinGpioStateb;
extern PIN_Handle hGpioPinb;

extern void bLed_createTask(void);

extern void bLed(uint8_t led, uint8_t nBlinks);



#endif /* BLED_H_ */
