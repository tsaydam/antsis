/*
 * bLed.c
 *
 *  Created on: 18 A�u 2015
 *      Author: Talha SAYDAM
 */

/*********************************************************************
 * INCLUDES
 */
#include <string.h>
#include <xdc/std.h>

#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Queue.h>
#ifdef POWER_SAVING
#include <ti/sysbios/family/arm/cc26xx/Power.h>
#include <ti/sysbios/family/arm/cc26xx/PowerCC2650.h>
#endif

#include <ICall.h>

#include "gatt.h"
#include "hci.h"
#include "gapgattserver.h"
#include "gattservapp.h"
#include "gapbondmgr.h"
#include "osal_snv.h"
#include "ICallBleAPIMSG.h"
#include "util.h"

#include "bsp_i2c.h"
#include "bsp_spi.h"
#include "driverlib/flash.h"
#include "driverlib/VIMS.h"

#include "sensor.h"
#include "board.h"
#include "accelerometerservice.h"
#include "devinfoservice-st.h"
#ifdef FEATURE_REGISTER_SERVICE
  #include "registerservice.h"
#endif

// Sensor devices
#include "st_util.h"
#include "sensortag_tmp.h"
#include "sensortag_hum.h"
#include "sensortag_bar.h"
#include "sensortag_mov.h"
#include "sensortag_opt.h"
#include "sensortag_keys.h"
#include "sensortag_io.h"
#include "bLed.h"
// Other devices
#include "ext_flash.h"
#include "ext_flash_layout.h"

// Optional services
#ifdef FEATURE_OAD
  #include "sensortag_connctrl.h"
  #include "oad_target.h"
  #include "oad.h"
#endif
/*********************************************************************
 * CONSTANTS
 */
// Advertising interval when device is discoverable (units of 625us, 160=100ms)
#define DEFAULT_ADVERTISING_INTERVAL          160

// Limited discoverable mode advertises for 30.72s, and then stops
// General discoverable mode advertises indefinitely
#define DEFAULT_DISCOVERABLE_MODE             GAP_ADTYPE_FLAGS_GENERAL

// Minimum connection interval (units of 1.25ms, 80=100ms) if automatic
// parameter update request is enabled
#define DEFAULT_DESIRED_MIN_CONN_INTERVAL     80

// Maximum connection interval (units of 1.25ms, 800=1000ms) if automatic
// parameter update request is enabled
#define DEFAULT_DESIRED_MAX_CONN_INTERVAL     800

// Slave latency to use if automatic parameter update request is enabled
#define DEFAULT_DESIRED_SLAVE_LATENCY         0

// Supervision timeout value (units of 10ms, 1000=10s) if automatic parameter
// update request is enabled
#define DEFAULT_DESIRED_CONN_TIMEOUT          1000

// Whether to enable automatic parameter update request when a connection is
// formed
#define DEFAULT_ENABLE_UPDATE_REQUEST         TRUE

// Connection Pause Peripheral time value (in seconds)
#define DEFAULT_CONN_PAUSE_PERIPHERAL         6

// How often to perform periodic event (in msec)
#define SBP_PERIODIC_EVT_PERIOD               5000

// Task configuration
#define SBP_TASK_PRIORITY                     1

#ifndef SBP_TASK_STACK_SIZE
#define SBP_TASK_STACK_SIZE                   644
#endif

// Internal Events for RTOS application
#define SBP_STATE_CHANGE_EVT                  0x0001
#define SBP_CHAR_CHANGE_EVT                   0x0002
#define SBP_PERIODIC_EVT                      0x0004
#ifdef FEATURE_OAD
#define SBP_OAD_WRITE_EVT                     0x0008
#endif //FEATURE_OAD

PIN_State pinGpioStateb;
PIN_Handle hGpioPinb;
/*********************************************************************
 * TYPEDEFS
 */

// App event passed from profiles.
typedef struct {
	uint8_t event;  // Which profile's event
	uint8_t status; // New status
} sbpEvt_t;



// Entity ID globally used to check for source and/or destination of messages
static ICall_EntityID selfEntity;

// Semaphore globally used to post events to the application thread
static ICall_Semaphore sem;

// Clock instances for internal periodic events.
static Clock_Struct periodicClock;

// Queue object used for app messages
static Queue_Struct appMsg;
static Queue_Handle appMsgQueue;

// events flag for internal application events.
static uint16_t events;

// Task configuration
Task_Struct sbpTaskb;
Char sbpTaskStackb[SBP_TASK_STACK_SIZE];



static void bLed_init(void);
static void bLed_taskFxn(UArg a0, UArg a1);


void bLed_createTask(void) {
	Task_Params taskParams;

	// Configure task
	Task_Params_init(&taskParams);
	taskParams.stack = sbpTaskStackb;
	taskParams.stackSize = 644;
	taskParams.priority = 1;

	Task_construct(&sbpTaskb, bLed_taskFxn, &taskParams, NULL);
}


static void bLed_init(void){
	ICall_registerApp(&selfEntity, &sem);

	appMsgQueue = Util_constructQueue(&appMsg);
	hGpioPinb = PIN_open(&pinGpioStateb, BoardGpioInitTable);
	bLed(IOID_10,10);
	// Create one-shot clocks for internal periodic events.
	//Util_constructClock(&periodicClock, SimpleBLEPeripheral_clockHandler,
	//SBP_PERIODIC_EVT_PERIOD, 0, false, SBP_PERIODIC_EVT);
}

static void bLed_taskFxn(UArg a0, UArg a1){

	bLed_init();

	for(;;){

		bLed(IOID_10,10);
	}
}


void bLed(uint8_t led, uint8_t nBlinks)
{
  uint8_t i;

  for (i=0; i<nBlinks; i++)
  {
    PIN_setOutputValue(hGpioPinb, led, Board_LED_ON);
    Task_sleep( ((200) * 1000) / Clock_tickPeriod );
    PIN_setOutputValue(hGpioPinb, led, Board_LED_OFF);
    Task_sleep( ((200) * 1000) / Clock_tickPeriod );
  }
}



